package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.List;

public class PizzaService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo){
        this.menuRepo=menuRepo;
        this.payRepo=payRepo;
    }

    public void readPayments(String filename){this.payRepo.readPayments(filename);}

    public List<MenuDataModel> getMenuData(){return menuRepo.getMenu();}

    public List<Payment> getPayments(){return payRepo.getAll(); }

    public void addPayment(int table, PaymentType type, double amount){
        if (table < 1 || table > 8)
            throw new IllegalArgumentException("Invalid table number");
        if (amount <= 0)
            throw new IllegalArgumentException("Invalid amount number");
        Payment payment= new Payment(table, type, amount);
        payRepo.add(payment);
    }

    public void addPayment(Payment payment) throws Exception {
        if (payment.getTableNumber() < 1 || payment.getTableNumber() > 8)
            throw new IllegalArgumentException("Invalid table number");
        if (payment.getAmount() <= 0)
            throw new IllegalArgumentException("Invalid amount number");
        payRepo.add(payment);
    }

    public double getTotalAmount(PaymentType type){
        double total=0.0f;
        List<Payment> l=getPayments();
        if ((l==null) ||(l.isEmpty()))
            return total;
        else {
            for (Payment p : l) {
                if (p.getType().equals(type))
                    if (p.getAmount()!=0)
                        total += p.getAmount();
            }
        }
        return total;
    }

}