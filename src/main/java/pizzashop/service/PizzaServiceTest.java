package pizzashop.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.stream.Stream;


@TestMethodOrder(MethodOrderer.Random.class)
class PizzaServiceTest {
    private PizzaService pizzaService;
    private MenuRepository menuRepository;
    private PaymentRepository paymentRepository;

    // Arrange
    @BeforeEach
    void setUp() {
        menuRepository = new MenuRepository();
        paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }


    @ParameterizedTest
    @Tag("valids")
    @DisplayName("Test valid => se adauga payment")
    @MethodSource(value = "validPaymentProvider")
    void testAddValidPayment(int table, PaymentType paymentType, double value){
        Assertions.assertDoesNotThrow(() -> pizzaService.addPayment(table, paymentType, value));
    }

    @ParameterizedTest
    @Tag("Invalids")
    @DisplayName("Test valid => nu se adauga payment")
    @MethodSource(value = "invalidPaymentProvider")
    void testAddInvalidPayment(int table, PaymentType paymentType, double value){
        Assertions.assertThrows(IllegalArgumentException.class, () ->
           pizzaService.addPayment(table, paymentType, value));
    }

    @ParameterizedTest
    @MethodSource(value = "getTotalProvider")
    void testGetTotal(String filename, PaymentType paymentType, double value){
        this.pizzaService.readPayments(filename);
        Assertions.assertEquals(value, pizzaService.getTotalAmount(paymentType));
    }

    private static Stream<Arguments> getTotalProvider(){
        return Stream.of(
                Arguments.of("data/test1.txt", PaymentType.Cash, 180.0f),
                Arguments.of("data/test1.txt", PaymentType.Card, 0.0f),
                Arguments.of("data/test2.txt", PaymentType.Cash, 0.0f),
                Arguments.of("data/test2.txt", PaymentType.Card, 180.0f),
                Arguments.of("data/test3.txt", PaymentType.Cash, 0.0f),
                Arguments.of("data/test3.txt", PaymentType.Card, 90.0f),
                Arguments.of("data/test4.txt", PaymentType.Card, 0.0f),
                Arguments.of("data/test4.txt", PaymentType.Cash, 0.0f)
        );
    }

    private static Stream<Arguments> validPaymentProvider() {
        return Stream.of(
                //TC01_ECP
                Arguments.of(1, PaymentType.Cash, 90),
                //TC05_ECP
                Arguments.of(1, PaymentType.Card, 30),
                //TC03_BVA
                Arguments.of(2, PaymentType.Cash, 1),
                //TC05_BVA
                Arguments.of(2, PaymentType.Card, Double.MAX_VALUE - 1)
        );
    }

    private static Stream<Arguments> invalidPaymentProvider() {
        return Stream.of(
                //TC02_ECP
                Arguments.of(-2, PaymentType.Cash, 90),
                //TC04_ECP
                Arguments.of(5, PaymentType.Cash, -20),
                //TC01_BVA
                Arguments.of(2, PaymentType.Cash, -1),
                //TC02_BVA
                Arguments.of(2, PaymentType.Cash, 0)
        );
    }
}