package teste.step1.step3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ServiceRepoEntityTest {
    private PaymentRepository paymentRepository;
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {
        paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(null, paymentRepository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addPayment_getTotalAmount_WithCash() throws Exception {
        Payment p1 = new Payment(3, PaymentType.Cash, 2);
        Payment p2 = new Payment(3, PaymentType.Card, 5);
        Payment p3 = new Payment(3, PaymentType.Cash, 5);

        pizzaService.addPayment(p1);
        pizzaService.addPayment(p2);
        pizzaService.addPayment(p3);
        double result = pizzaService.getTotalAmount(PaymentType.Cash);

        assertEquals(7, result);
    }

    @Test
    void addPayment_getTotalAmount_WithCard() throws Exception {
        //Arrange
        Payment p1 = new Payment(3, PaymentType.Card, 2);
        Payment p2 = new Payment(3, PaymentType.Card, 5);
        Payment p3 = new Payment(3, PaymentType.Card, 5);

        //Act
        pizzaService.addPayment(p1);
        pizzaService.addPayment(p2);
        pizzaService.addPayment(p3);
        double result = pizzaService.getTotalAmount(PaymentType.Cash);

        //Assert
        assertEquals(0, result);
    }
}
