package teste.step1.step2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

public class RepoServiceTest {
    private PaymentRepository paymentRepository;
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {
        paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(null, paymentRepository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addPayment_When_ValidInput_IsAdded() throws Exception {
        Payment payment = mock(Payment.class);
        Mockito.when(payment.getTableNumber()).thenReturn(3);
        Mockito.when(payment.getAmount()).thenReturn(20.0);
        Mockito.when(payment.getType()).thenReturn(PaymentType.Cash);

        pizzaService.addPayment(payment);
        pizzaService.addPayment(payment);
        List<Payment> payments = pizzaService.getPayments();

        assertEquals(2, payments.size());
    }

    @Test
    void addPayment_When_Table_LessThan1_IsNotAdded() throws Exception {
        Payment payment = mock(Payment.class);
        Mockito.when(payment.getTableNumber()).thenReturn(-3);

        Exception exception = assertThrows(Exception.class, () -> {
            pizzaService.addPayment(payment);
        });
        List<Payment> payments = pizzaService.getPayments();
        assertEquals("Invalid table number", exception.getMessage());
        assertEquals(0, payments.size());
    }
}
