package teste.step1;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class PizzaServiceTest {
    private PaymentRepository mockitoPaymentRepository;
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {
        mockitoPaymentRepository = mock(PaymentRepository.class);
        pizzaService = new PizzaService(null, mockitoPaymentRepository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addPayment_When_ValidInput_PayRepo_Add_IsCalledOnce() throws Exception {
        pizzaService.addPayment(4, PaymentType.Card, 10);
        Mockito.verify(mockitoPaymentRepository, Mockito.times(1)).add(any());
    }

    @Test
    void addPayment_When_Table_LessThan1_ThrowsException() {
        Exception exception = assertThrows(Exception.class, () -> {
            pizzaService.addPayment(-2, PaymentType.Card, 10);
        });
        assertEquals("Invalid table number", exception.getMessage());
        Mockito.verify(mockitoPaymentRepository, Mockito.times(0)).add(any());
    }

    @Test
    void addPayment_When_AmountLessThan1_ThrowsException() {
        Exception exception = assertThrows(Exception.class, () -> {
            pizzaService.addPayment(2, PaymentType.Card, -4);
        });
        assertEquals("Invalid amount number", exception.getMessage());
        Mockito.verify(mockitoPaymentRepository, Mockito.times(0)).add(any());
    }
}
