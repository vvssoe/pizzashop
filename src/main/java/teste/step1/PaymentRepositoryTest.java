package teste.step1;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;

import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class PaymentRepositoryTest {

    PaymentRepository paymentRepository;

    @BeforeEach
    void setUp() {
        paymentRepository = new PaymentRepository();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void add() {
        Payment p1 = mock(Payment.class);
        Mockito.when(p1.getTableNumber()).thenReturn(1);
        Mockito.when(p1.getType()).thenReturn(PaymentType.Cash);
        Mockito.when(p1.getAmount()).thenReturn(30.0);
        Mockito.when(p1.toString()).thenReturn("1, Cash, 30.0");

        Payment p2 = mock(Payment.class);
        Mockito.when(p2.getTableNumber()).thenReturn(2);
        Mockito.when(p2.getType()).thenReturn(PaymentType.Card);
        Mockito.when(p2.getAmount()).thenReturn(30.0);
        Mockito.when(p2.toString()).thenReturn("2, Cash, 30.0");

        paymentRepository.add(p1);
        paymentRepository.add(p2);

        assertEquals(2, paymentRepository.getAll().size());
        assertEquals(30.0, paymentRepository.getAll().get(1).getAmount());
        assertEquals(PaymentType.Cash, paymentRepository.getAll().get(0).getType());
    }

    @Test
    void getAll() {
        Payment p1 = mock(Payment.class);
        Payment p2 = mock(Payment.class);

        paymentRepository.add(p1);
        paymentRepository.add(p2);

        List<Payment> result = paymentRepository.getAll();

        assertEquals(2, result.size());
        assertEquals(p1, result.get(0));
        assertEquals(p2, result.get(1));
    }
}
