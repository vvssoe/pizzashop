package teste.step1;

import org.junit.jupiter.api.*;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PaymentTest {
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void get_set_tableNumber() {
        Payment payment = new Payment(1, PaymentType.Card, 1);
        int originalTableNumber = payment.getTableNumber();
        payment.setTableNumber(3);
        int modifiedTableNumber = payment.getTableNumber();
        assertEquals(1, originalTableNumber);
        assertEquals(3, modifiedTableNumber);
    }

    @Test
    void get_set_type() {
        Payment payment = new Payment(1, PaymentType.Card, 1);
        PaymentType originalType = payment.getType();
        payment.setType(PaymentType.Cash);
        PaymentType modifiedType = payment.getType();
        assertEquals(PaymentType.Card, originalType);
        assertEquals(PaymentType.Cash, modifiedType);
    }
}
